/*
 ============================================================================
 Name        : Trab2ProgLin.c
 Author      : Eduardo Motta de Oliveira
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

enum Status { ERRO_ENTRA, ERRO_SAI, SUCESSO};

void printTableau(double** tableau, int m, int n) {
	printf("\n");
	for (int j = 0; j < n-2; j++) {
		printf(" X%d\t", j);
	}
	printf(" b\t\n");

	for (int j = 0; j < n; j++)
		printf("--------");
	printf("\n");

	for (int j = 0; j < n-1; j++) {
		printf("%0.2lf\t", tableau[0][j]);
	}
	printf("Z\n");

	for (int j = 0; j < n; j++)
		printf("--------");
	printf("\n");

	for (int i = 1; i < m; i++) {
		for (int j = 0; j < n-1; j++) {
			printf("%0.2lf\t", tableau[i][j]);
		}
		int varBasica = (int)tableau[i][n-1];
		if (varBasica < 0) {
			printf("-\n");
		}
		else {
			printf("X%d\n", varBasica);
		}
	}
}

double** alocarTableau(int m, int n) {
	double** tableau = (double**) malloc(m * sizeof(double*));
	for (int i = 0; i < m; i++) {
		tableau[i] = (double*) calloc(n, sizeof(double));
	}
	return tableau;
}

void freeTableau(double** tableau, int m, int n) {
	for (int i = 0; i < m; i++) {
		free(tableau[i]);
	}
	free(tableau);
}

int ehIgual(double a, double b) {
	return (fabs(a - b) < 0.000001);
}

int ehZero(double a) {
	return (fabs(a - 0.0) < 0.000001);
}

void trocaSinalLinha(double** tableau, int linha, int n) {
	for (int j = 0; j < n-1; j++) {
		if (!ehZero(tableau[linha][j])) {
			tableau[linha][j] *= (-1);
		}
	}
}

int maiorPositivoLinha(int linha, double** tableau, int n) {
	int indiceMaior = 0;
	for (int j = 0; j < n-2; j++) {
		if (tableau[linha][j] > tableau[linha][indiceMaior]) {
			indiceMaior = j;
		}
	}
	if (tableau[linha][indiceMaior] > 0.0) {
		return indiceMaior;
	}
	return -1;
}

int maisNegativoColB(double** tableau, int m, int n) {
	int indiceMenor = 1;
	for (int i = 1; i < m; i++) {
		if (tableau[i][n-2] < tableau[indiceMenor][n-2]) {
			indiceMenor = i;
		}
	}
	if (tableau[indiceMenor][n-2] < 0.0) {
		return indiceMenor;
	}
	return -1;
}

int ehVariavelBasica(int var, double** tableau, int m, int n) {
	for (int i = 1; i < m; i++) {
		if ( var == (int)tableau[i][n-1] ) {
			return  1;
		}
	}
	return 0;
}

// Retorna indice da linha com menor divisao > 0
int menorDivisaoPositivaLinLin(double** tableau, int m, int n, int linY) {
	int indiceMenorDivisao = -1;
	double divisao, menorDivisao = 100000000000000;

	for (int j = n-3; j >= 0; j--) {

		if ( !ehVariavelBasica(j, tableau, m, n) ) {

			if (tableau[0][j] > 0.0) {
				return -1;
			}

			if ( !ehZero(tableau[linY][j]) ) {

				if ( ehZero(tableau[0][linY]) ) {
					if ( 0.0 < menorDivisao ) {
						menorDivisao = 0.0;
						indiceMenorDivisao = j;
					}
				}
				else {
					divisao = tableau[0][j]/tableau[linY][j];
					if ( divisao <= menorDivisao && divisao >= 0.0) {    // CUIDADO!  >= 0 pode dar problema
						menorDivisao = divisao;
						indiceMenorDivisao = j;
					}
				}
			}

		}
	}
	return indiceMenorDivisao;
}

void divideLinhaPorConstante(double** tableau, int linha, int n, double constante) {
	for (int j = 0; j < n-1; j++) {
		if ( !ehZero(tableau[linha][j])) {
			tableau[linha][j] /= constante;
		}
	}
}

void pivoteia(double** tableau, int m, int n, int iPivot, int jPivot) {
	for (int i = 0; i < m; i++) {
		if (i != iPivot) {
			double c = tableau[i][jPivot]/tableau[iPivot][jPivot];
			for (int j = 0; j < n-1; j++) {
				//printf("%lf - (%lf * %lf)\n", tableau[i][j], c ,tableau[iPivot][j]);
				tableau[i][j] -= c * tableau[iPivot][j];
			}
		}
	}
}

int trocaVariavelNaBase(double** tableau, int m, int n) {
	int i = maisNegativoColB(tableau, m, n);
	if (i == -1) {
		return ERRO_SAI;
	}
	int j = menorDivisaoPositivaLinLin(tableau, m, n, i);

	if (j == -1) {
		return ERRO_ENTRA;
	}

	printf("\nNovo pivot: (%d, %d)\n", i, j);
	printf("Divide linha %d por %0.2lf e pivoteia\n", i, tableau[i][j]);
	divideLinhaPorConstante(tableau, i, n, tableau[i][j]);
	tableau[i][n-1] = (double)j;

	pivoteia(tableau, m, n, i, j);
	printTableau(tableau, m, n);

	return SUCESSO;
}

// Se canonica retorna posicao do 1, senao retorna -1
int isColCanonica(double** tableau, int j, int iInicio, int iFim) {

	double soma = 0.0;
	for (int i = iInicio; i <= iFim; i++) {
		soma += tableau[i][j];
	}

	if (ehIgual(soma, 1.0)) {
		// procura pos do 1 ou verifica se so ha um 1
		int posUm = -1;
		for (int i = iInicio; i <= iFim; i++) {

			if (ehIgual(tableau[i][j], 1.0)) {

				if (posUm == -1) {
					posUm = i;

				}
				else {
					return -1;
				}
			}
		}
		// Base canonica ok. Retornando pos 1
		return posUm;
	}
	else {
		return -1;
	}
	return -1;
}

void printSolucao(double** tableau, int m, int n) {
	double valorVar;
	printf("\n x* = ( ");
	for (int x = 0; x < n-2; x++) {
		for (int i = 1; i < m; i++) {
			valorVar = 0.0;
			if ( ehIgual(tableau[i][n-1], (double)x) ){
				valorVar = tableau[i][n-2];
				break;
			}
		}
		printf("%0.3lf ", valorVar);
	}
	printf(")\n");
	printf(" z* = %0.3lf\n", tableau[0][n-2]);
}


int existeVarNaoBasicaComZNulo(double** tableau, int m, int n) {
	int i, j, isBasica;
	for (j = 0; j < n-2; j++) {  // para cada var
		isBasica = -1;

		for (i = 1; i < m; i++) {

			if ( j == (int)tableau[i][n-1] ) {
				isBasica = 1;
				break;
			}
		}

		if (isBasica == -1) {
			if ( ehZero(tableau[0][j]) ) {
				return 1;
			}
		}
	}
	return 0;
}

int existeVarBasicaComValorNulo(double** tableau, int m, int n) {
	for (int i = 1; i < m; i++) {
		if (ehZero(tableau[i][n-2])) {
			return 1;
		}
	}
	return 0;
}

/************************
 * 		   MAIN			*
 ************************/

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("\n Erro! Sem arquivo de entrada.");
		printf("\n Modo de usar: ./dualSimplex <arquivo_do_problema> \n");
		return 0;
	}

	int m = 0, n = 0;
	double** tableau;

	// LEITURA
	char* buffer = (char*) calloc(256, sizeof(double));
	FILE* arq = fopen(argv[1], "r");
	if (arq == NULL) {
		printf("Erro: Arquivo nao encontrado para leitura.");
		exit(1);
	}

	fscanf(arq,"%d %d", &m, &n);
	n++; // Para acomodar indices das var basicas
	tableau = alocarTableau(m, n);
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n-1; j++) {
			fscanf(arq,"%lf", &tableau[i][j]);
		}
	}

	free(buffer);
	fclose(arq);
	int status;


	//------------------//
	//   DUAL SIMPLEX	//
	//------------------//

	trocaSinalLinha(tableau, 0, n);

	// Preenche col var basicas
	int posUm;
	for (int j = 0; j < n-2; j++) {
		posUm = isColCanonica(tableau, j, 1, m-1);
		if (posUm > 0) {
			tableau[posUm][n-1] = (double)j;
		}
	}
	printf("m = %d, n = %d\n\n", m, n);
	printf("Inicio:\n");
	printTableau(tableau, m, n);

	status = SUCESSO;
	int i = 0;
	while (status == SUCESSO && i < 10) {
		status = trocaVariavelNaBase(tableau, m, n);
		i++;
	}


	switch (status) {
		case ERRO_SAI:
			if (existeVarNaoBasicaComZNulo(tableau, m, n)) {
				printf("\nSimplex terminado: Solucao Multipla (infinitas solucoes)\nUma solucao otima:");
			}
			else if (existeVarBasicaComValorNulo(tableau, m, n)) {
				printf("\nSimplex terminado: Solucao Degenerada\n");
			}
			else {
				printf("\nSimplex terminado. (Solucao unica)\n");
			}
			printSolucao(tableau, m, n);
			printf("\nQuadro final:\n");
			printTableau(tableau, m, n);
			freeTableau(tableau, m, n);
			exit(0);
			break;
		case ERRO_ENTRA:
			printf("\nSimplex terminado: Sem solucao (Solucao ilimitada para o Dual)\n");
			printf("\nQuadro final:\n");
			printTableau(tableau, m, n);
			freeTableau(tableau, m, n);
			exit(0);
			break;
	}








	return EXIT_SUCCESS;
}
